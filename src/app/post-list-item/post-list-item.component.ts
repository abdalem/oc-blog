import { Component, Input, OnInit } from '@angular/core';
import { Post } from '../models/post.model';
import { PostsService } from '../services/posts.service';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {
  
  @Input() item: Post;
  @Input() index: number;

  constructor(private postsService: PostsService) { }

  ngOnInit() {
  }

  onDontLoveIt() {
    this.postsService.loveIt(this.index);
  }

  onLoveIt() {
    this.postsService.dontLoveIt(this.index);
  }

  onDelete() {
    this.postsService.remove(this.index);
  }
}
