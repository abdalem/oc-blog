import { Injectable } from '@angular/core';
import { Post } from '../models/post.model';
import { Subject } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  private posts = [];
  postsSubject = new Subject<any[]>();

  constructor() { }

  emit() {
    this.postsSubject.next(this.posts.slice());
  }

  getPosts() {
    this.emit();
  }

  new(post: Post) {
    this.posts.push(post);
    this.emit();
  }

  remove(index: number) {
    this.posts.splice(index, 1);
    this.emit();
  }

  loveIt(index: number) {
    this.posts[index].loveIts ++;
    this.emit();
  }

  dontLoveIt(index: number) {
    this.posts[index].loveIts --;
    this.emit();
  }
}
