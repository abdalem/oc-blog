import { Component, OnInit } from '@angular/core';
import { PostsService } from '../services/posts.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Post } from '../models/post.model';

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.scss']
})
export class NewPostComponent implements OnInit {
  postForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private postsService: PostsService, private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.postForm = this.formBuilder.group({
      title: ['', Validators.required],
      content: ['Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut accumsan felis at arcu pulvinar consequat. Cras lectus tortor, ultrices vel lacus sed, venenatis scelerisque dui. Vestibulum vel rhoncus turpis. Nunc eu commodo felis. In gravida a nunc at placerat. Fusce et nunc faucibus, laoreet diam et, facilisis turpis.', Validators.required]
    })
  }

  onSubmit() {
    const title = this.postForm.get('title').value;
    const content = this.postForm.get('content').value;
    const post = new Post(title, content, 0, new Date());

    this.postsService.new(post);
    this.router.navigate(['/posts']);
  }

}
